package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
    private List<Flower> flowers = new ArrayList<>();
    DocumentBuilder db;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void parseFlowers() {

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        try {
            Document doc = db.parse(xmlFileName);
            Element root = doc.getDocumentElement();
            NodeList flowersList = root.getElementsByTagName("flower");
            for (int i = 0; i < flowersList.getLength(); i++) {
                Element flowerElement = (Element) flowersList.item(i);
                Flower flower = createFlower(flowerElement);
                flowers.add(flower);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }
    }

    private Flower createFlower(Element flowerElement) {
        Flower flower = new Flower();
        // name
        flower.setName(flowerElement.getElementsByTagName("name").item(0).getTextContent());
        // soil
        flower.setSoil(flowerElement.getElementsByTagName("soil").item(0).getTextContent());
        // origin
        flower.setOrigin(flowerElement.getElementsByTagName("origin").item(0).getTextContent());
        // visualParameters
        Flower.VisualParameters visualParameters = flower.getVisualParameters();
        Element visualElement = (Element) flowerElement.getElementsByTagName("visualParameters").item(0);
        // stemColour
        visualParameters.setStemColour(visualElement.getElementsByTagName("stemColour").item(0).getTextContent());
        // leafColour
        visualParameters.setLeafColour(visualElement.getElementsByTagName("leafColour").item(0).getTextContent());
        // aveLenFlower
        Flower.VisualParameters.AveLenFlower aveLenFlower = visualParameters.getAveLenFlower();
        Element aveLenFlowerElement = (Element) visualElement.getElementsByTagName("aveLenFlower").item(0);
        aveLenFlower.setMeasure(aveLenFlowerElement.getAttribute("measure"));
        aveLenFlower.setValue(Integer.valueOf(aveLenFlowerElement.getTextContent()));
        visualParameters.setAveLenFlower(aveLenFlower);
        flower.setVisualParameters(visualParameters);
        // growingTips
        Flower.GrowingTips growingTips = flower.getGrowingTips();
        Element growingElement = (Element) flowerElement.getElementsByTagName("growingTips").item(0);
        // Temperature
        Flower.GrowingTips.Temperature temperature = growingTips.getTemperature();
        Element temperatureElement = (Element) growingElement.getElementsByTagName("tempreture").item(0);
        temperature.setMeasure(temperatureElement.getAttribute("measure"));
        temperature.setValue(Integer.valueOf(temperatureElement.getTextContent()));
        growingTips.setTemperature(temperature);
        // Lightning
        Flower.GrowingTips.Lighting lighting = growingTips.getLighting();
        Element lightningElement = (Element) growingElement.getElementsByTagName("lighting").item(0);
        lighting.setLightRequiring(lightningElement.getAttribute("lightRequiring"));
        growingTips.setLighting(lighting);
        // Watering
        Flower.GrowingTips.Watering watering = growingTips.getWatering();
        Element wateringElement = (Element) growingElement.getElementsByTagName("watering").item(0);
        watering.setMeasure(wateringElement.getAttribute("measure"));
        watering.setValue(Integer.valueOf(wateringElement.getTextContent()));
        growingTips.setWatering(watering);
        // Multiplying
        flower.setMultiplying(flowerElement.getElementsByTagName("multiplying").item(0).getTextContent());

        return flower;
    }

    public void writeToXML(List<Flower> flowers, String fileName) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = db.newDocument();
        Element root = doc.createElement("flowers");
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        doc.appendChild(root);
        Element element;
        for (Flower f : flowers) {
            Element flower = doc.createElement("flower");
            element = doc.createElement("name");
            element.setTextContent(f.getName());
            flower.appendChild(element);
            element = doc.createElement("soil");
            element.setTextContent(f.getSoil());
            flower.appendChild(element);
            element = doc.createElement("origin");
            element.setTextContent(f.getOrigin());
            flower.appendChild(element);

            Element visualParameters = doc.createElement("visualParameters");
            element = doc.createElement("stemColour");
            element.setTextContent(f.getVisualParameters().getStemColour());
            visualParameters.appendChild(element);
            element = doc.createElement("leafColour");
            element.setTextContent(f.getVisualParameters().getLeafColour());
            visualParameters.appendChild(element);
            element = doc.createElement("aveLenFlower");
            element.setTextContent(String.valueOf(f.getVisualParameters().getAveLenFlower().getValue()));
            element.setAttribute("measure", f.getVisualParameters().getAveLenFlower().getMeasure());
            visualParameters.appendChild(element);

            flower.appendChild(visualParameters);

            Element growingTips = doc.createElement("growingTips");
            element = doc.createElement("tempreture");
            element.setTextContent(String.valueOf(f.getGrowingTips().getTemperature().getValue()));
            element.setAttribute("measure", f.getGrowingTips().getTemperature().getMeasure());
            growingTips.appendChild(element);
            element = doc.createElement("lighting");
            element.setAttribute("lightRequiring", f.getGrowingTips().getLighting().getLightRequiring());
            growingTips.appendChild(element);
            element = doc.createElement("watering");
            element.setTextContent(String.valueOf(f.getGrowingTips().getWatering().getValue()));
            element.setAttribute("measure", f.getGrowingTips().getWatering().getMeasure());
            growingTips.appendChild(element);

            flower.appendChild(growingTips);

            element = doc.createElement("multiplying");
            element.setTextContent(f.getMultiplying());
            flower.appendChild(element);

            root.appendChild(flower);
        }
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");

        try {
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new FileWriter(fileName));
            transformer.transform(source, result);
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }

    }



    // PLACE YOUR CODE HERE

}
