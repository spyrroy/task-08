package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private List<Flower> flowers = new ArrayList<>();
	private Flower flower;
	String currentTag;
	String currentTagAttr;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public List<Flower> getFlowers() {
		return flowers;
	}

	DefaultHandler defaultHandler = new DefaultHandler() {
		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equalsIgnoreCase("flower")) {
				flower = new Flower();
			} else {
				currentTag = qName;
				currentTagAttr = attributes.getValue(0);
			}
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			if (qName.equalsIgnoreCase("flower")) {
				flowers.add(flower);
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			String data = new String(ch, start, length).strip();
			if (currentTag != null) {
				if (currentTag.equalsIgnoreCase("name")) {
					flower.setName(data);
				}
				if (currentTag.equalsIgnoreCase("soil")) {
					flower.setSoil(data);
				}
				if (currentTag.equalsIgnoreCase("origin")) {
					flower.setOrigin(data);
				}
				if (currentTag.equalsIgnoreCase("stemColour")) {
					flower.getVisualParameters().setStemColour(data);
				}
				if (currentTag.equalsIgnoreCase("leafColour")) {
					flower.getVisualParameters().setLeafColour(data);
				}
				if (currentTag.equalsIgnoreCase("aveLenFlower")) {
					String measure = flower.getVisualParameters().getAveLenFlower().getMeasure();
					flower.getVisualParameters().getAveLenFlower().setMeasure(measure);
					flower.getVisualParameters().getAveLenFlower().setValue(Integer.valueOf(data));
				}
				if (currentTag.equalsIgnoreCase("tempreture")) {
					String measure = flower.getGrowingTips().getTemperature().getMeasure();
					flower.getGrowingTips().getTemperature().setMeasure(measure);
					flower.getGrowingTips().getTemperature().setValue(Integer.valueOf(data));
				}
				if (currentTag.equalsIgnoreCase("lighting")) {
					flower.getGrowingTips().getLighting().setLightRequiring(currentTagAttr);
				}
				if (currentTag.equalsIgnoreCase("watering")) {
					String measure = flower.getGrowingTips().getWatering().getMeasure();
					flower.getGrowingTips().getWatering().setMeasure(measure);
					flower.getGrowingTips().getWatering().setValue(Integer.valueOf(data));
				}
				if (currentTag.equalsIgnoreCase("multiplying")) {
					flower.setMultiplying(data);
				}

			}
			currentTag = null;
		}
	};



	public void parseFlowers() throws SAXNotSupportedException, SAXNotRecognizedException, ParserConfigurationException {
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		saxParserFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		try {
			SAXParser saxParser = saxParserFactory.newSAXParser();
			saxParser.parse(xmlFileName, defaultHandler);
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}


	}



}