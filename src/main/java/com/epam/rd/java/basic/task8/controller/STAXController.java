package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private List<Flower> flowers = new ArrayList<>();
	private Flower flower;
	String currentTag;
	String currentTagAttr;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> getFlowers() {
		return flowers;
	}
	public void parseFlowers() {
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		inputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		try {
			XMLEventReader reader = inputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					if (startElement.getName().getLocalPart().equalsIgnoreCase("flower")) {
						flower = new Flower();
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("name")) {
						flower.setName(reader.nextEvent().asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("soil")) {
						flower.setSoil(reader.nextEvent().asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("origin")) {
						flower.setOrigin(reader.nextEvent().asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("stemColour")) {
						flower.getVisualParameters().setStemColour(reader.nextEvent().asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("leafColour")) {
						flower.getVisualParameters().setLeafColour(reader.nextEvent().asCharacters().getData());
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("aveLenFlower")) {
						String measure = flower.getVisualParameters().getAveLenFlower().getMeasure();
						flower.getVisualParameters().getAveLenFlower().setMeasure(measure);
						flower.getVisualParameters().getAveLenFlower().setValue(Integer.valueOf(reader.nextEvent().asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("tempreture")) {
						String measure = flower.getGrowingTips().getTemperature().getMeasure();
						flower.getGrowingTips().getTemperature().setMeasure(measure);
						flower.getGrowingTips().getTemperature().setValue(Integer.valueOf(reader.nextEvent().asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("lighting")) {
						flower.getGrowingTips().getLighting().setLightRequiring(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("watering")) {
						String measure = flower.getGrowingTips().getWatering().getMeasure();
						flower.getGrowingTips().getWatering().setMeasure(measure);
						flower.getGrowingTips().getWatering().setValue(Integer.valueOf(reader.nextEvent().asCharacters().getData()));
					} else if (startElement.getName().getLocalPart().equalsIgnoreCase("multiplying")) {
						flower.setMultiplying(reader.nextEvent().asCharacters().getData());
					}
				}
				if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					if (endElement.getName().getLocalPart().equalsIgnoreCase("flower")) {
						flowers.add(flower);
					}
				}
			}
		} catch (XMLStreamException | FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
