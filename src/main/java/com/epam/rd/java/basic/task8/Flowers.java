package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.List;


public class Flowers {

    private List<Flower> flowers = new ArrayList<>();

    public void add(Flower flower) {
        flowers.add(flower);
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "flowers=" + flowers +
                '}';
    }
}
