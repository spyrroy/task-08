package com.epam.rd.java.basic.task8;




public class Flower {

    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }

    public Flower() {
    }

    public Flower(String name, String soil, String origin, VisualParameters visualParameters, GrowingTips growingTips, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.visualParameters = visualParameters;
        this.growingTips = growingTips;
        this.multiplying = multiplying;
    }

    private String name;

    private String soil;

    private String origin;

    private VisualParameters visualParameters = new VisualParameters();

    private GrowingTips growingTips = new GrowingTips();

    private String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public static class VisualParameters {
        @Override
        public String toString() {
            return "VisualParameters{" +
                    "stemColour='" + stemColour + '\'' +
                    ", leafColour='" + leafColour + '\'' +
                    ", aveLenFlower=" + aveLenFlower +
                    '}';
        }

        private String stemColour;
        private String leafColour;
        private AveLenFlower aveLenFlower = new AveLenFlower();

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public AveLenFlower getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(AveLenFlower aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }

        public static class AveLenFlower {

            private String measure;

            private Integer value;

            public String getMeasure() {
                if (measure == null) {
                    return "cm";
                } else {
                    return measure;
                }
            }

            public void setMeasure(String measure) {
                this.measure = measure;
            }

            public Integer getValue() {
                return value;
            }

            public void setValue(Integer value) {
                this.value = value;
            }

            @Override
            public String toString() {
                return "AveLenFlower{" +
                        "measure='" + measure + '\'' +
                        ", value=" + value +
                        '}';
            }
        }


    }


    public static class GrowingTips {
        @Override
        public String toString() {
            return "GrowingTips{" +
                    "temperature=" + temperature +
                    ", lighting=" + lighting +
                    ", watering=" + watering +
                    '}';
        }

        private Temperature temperature = new Temperature();

        private Lighting lighting = new Lighting();

        private Watering watering = new Watering();

        public Temperature getTemperature() {
            return temperature;
        }

        public void setTemperature(Temperature temperature) {
            this.temperature = temperature;
        }

        public Lighting getLighting() {
            return lighting;
        }

        public void setLighting(Lighting lighting) {
            this.lighting = lighting;
        }

        public Watering getWatering() {
            return watering;
        }

        public void setWatering(Watering watering) {
            this.watering = watering;
        }

        public static class Temperature {
            @Override
            public String toString() {
                return "Temperature{" +
                        "measure='" + measure + '\'' +
                        ", value=" + value +
                        '}';
            }

            private String measure;
            private Integer value;

            public String getMeasure() {
                if (measure == null) {
                    return "celcius";
                } else {
                    return measure;
                }
            }

            public void setMeasure(String measure) {
                this.measure = measure;
            }

            public Integer getValue() {
                return value;
            }

            public void setValue(Integer value) {
                this.value = value;
            }
        }

        public static class Lighting {
            @Override
            public String toString() {
                return "Lighting{" +
                        "lightRequiring='" + lightRequiring + '\'' +
                        '}';
            }

            private String lightRequiring;

            public String getLightRequiring() {
                return lightRequiring;
            }

            public void setLightRequiring(String lightRequiring) {
                this.lightRequiring = lightRequiring;
            }
        }


        public static class Watering {
            @Override
            public String toString() {
                return "Watering{" +
                        "measure='" + measure + '\'' +
                        ", value=" + value +
                        '}';
            }

            private String measure;
            private Integer value;

            public String getMeasure() {
                if (measure == null) {
                    return "mlPerWeek";
                } else {
                    return measure;
                }
            }

            public void setMeasure(String measure) {
                this.measure = measure;
            }

            public Integer getValue() {
                return value;
            }

            public void setValue(Integer value) {
                this.value = value;
            }
        }

    }

}
